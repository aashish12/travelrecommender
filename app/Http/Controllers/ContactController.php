<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ContactController extends Controller
{
    //
    public function displayContact(){
        $abc = 'changed value';
        $age = 15 ;

        //using compact function
        // return view('contact.contact', compact('age'));
        
        //using with function
        // return view('contact.contact')->with(['age', $age']);

        //using arry inside view function
        return view('contact.contact',['age' => $age]);

    } 

    public function displayCallme(){
        return view('contact.call.callme');

    }
}
