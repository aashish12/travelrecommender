<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function showDashboard(){
        return view('dashboard.index');
    }
    public function showUsers(){
        return view('dashboard.users');
    }
    public function addUser(){
        return view('dashboard.createusers');
    }
    public function editUsers(){
        
    }
    public function editUserDetail(){
        return view('dashboard.editusers');        
    }
    public function editUserType(){
        
    }
}
