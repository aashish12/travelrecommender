<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AboutController extends Controller
{
    public function showAboutContent(){
        return view('about');
    }

    public function getAllProjects(){
        $age = 20;
        // compact
        // return view('about.projects', compact('abc'));
        // return view('about.projects')->with('bbc' , $abc);
        return view('about.projects',['age' => $age]);


    }
}
